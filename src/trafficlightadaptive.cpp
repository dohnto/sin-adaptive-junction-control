/*
 * File:   trafficlightadaptive.cpp
 * Author: daniel bokis, tomas dohnalek
 *
 * Created on 26. listopad 2014, 14:36
 */

#include "trafficlightadaptive.h"
#include "utils.h"
#include <algorithm>    // std::max

TrafficLightAdaptive::TrafficLightAdaptive(TrafficLightIdentification id)
    : ITrafficLight(id) {
    if (TRAFFIC_LIGHT_ADAPTIVE_FUZZY_LOGIC == DIFF_TIME)
        fuzzy = new FuzzyDiffWaitingTime();
    else
        fuzzy = new FuzzyMaxWaitingTime();
    fuzzy->finishInit();
}

LightStatus TrafficLightAdaptive::getNextStatus() {
    if (Time - timeWhenStatusChanged >= TRAFFIC_LIGHT_MIN_TIME_BETWEEN_SWITCH) {
        unsigned qNorthSize = qNorth.size();
        double   qNorthMaxTime = getMaxTimeQ(qNorth);

        unsigned qSouthSize = qSouth.size();
        double   qSouthMaxTime = getMaxTimeQ(qSouth);

        unsigned qWestSize = qWest.size();
        double   qWestMaxTime = getMaxTimeQ(qWest);

        unsigned qEastSize = qEast.size();
        double   qEastMaxTime = getMaxTimeQ(qEast);

        unsigned redQueSize, greenQueSize;
        double maxWaitTime;


        TrafficLightAdaptiveConnectionCalculation addition = TRAFFIC_LIGHT_ADAPTIVE_CONNECTION_CALCULATION;

        double coef = 3.0;
        if (status == GOHORIZONTAL) {
            redQueSize = qNorthSize + qSouthSize;
            greenQueSize = qEastSize + qWestSize;


            if (id == ID_WEST) {
                if (addition == ALL)
                    greenQueSize += Utils::connectionRoadToWest.size();

                if (addition == MEAN) {
                    greenQueSize += Utils::countOfCarsCommingBellow(Utils::connectionRoadToWest, BETWEEN_JUNCTIONS_MEAN_SEC - coef*BETWEEN_JUNCTIONS_VARIANCE);
                }
            } else {
                if (addition == ALL)
                    greenQueSize += Utils::connectionRoadToEast.size();
                if (addition == MEAN)
                    greenQueSize += Utils::countOfCarsCommingBellow(Utils::connectionRoadToEast,  BETWEEN_JUNCTIONS_MEAN_SEC - coef*BETWEEN_JUNCTIONS_VARIANCE);
            }

            if (TRAFFIC_LIGHT_ADAPTIVE_FUZZY_LOGIC == DIFF_TIME) {
                maxWaitTime = max(qWestMaxTime, qEastMaxTime) - max(qSouthMaxTime, qNorthMaxTime);
            } else {
                maxWaitTime = max(qSouthMaxTime, qNorthMaxTime);
            }
        } else {
            redQueSize = qWestSize + qEastSize;
            greenQueSize = qNorthSize + qSouthSize;

            if (id == ID_WEST) {
                if (addition == ALL)
                    redQueSize += Utils::connectionRoadToWest.size();
                if (addition == MEAN)
                    redQueSize += Utils::countOfCarsCommingBellow(Utils::connectionRoadToWest, BETWEEN_JUNCTIONS_MEAN_SEC - coef*BETWEEN_JUNCTIONS_VARIANCE);
            } else {
                if (addition == ALL)
                    redQueSize += Utils::connectionRoadToEast.size();
                if (addition == MEAN)
                    redQueSize += Utils::countOfCarsCommingBellow(Utils::connectionRoadToEast, BETWEEN_JUNCTIONS_MEAN_SEC - coef*BETWEEN_JUNCTIONS_VARIANCE);
            }


            if (TRAFFIC_LIGHT_ADAPTIVE_FUZZY_LOGIC == DIFF_TIME) {
                maxWaitTime = max(qSouthMaxTime, qNorthMaxTime) - max(qWestMaxTime, qEastMaxTime);
            } else {
                maxWaitTime = max(qWestMaxTime, qEastMaxTime);
            }
        }

        bool change = fuzzy->getChange(redQueSize, greenQueSize, maxWaitTime);

        if (change) {
            timeWhenStatusChanged = Time;
            return (status == GOHORIZONTAL) ? GOVERTICAL : GOHORIZONTAL;
        } else {
            return status;
        }
    } else {
        return status;
    }

}
