/*
 * File:   utils.h
 * Author: daniel bokis, tomas dohnalek
 *
 * Created on 26. listopad 2014, 14:36
 */

#ifndef UTILS_H
#define UTILS_H

#include "types.h"
#include "itrafficlight.h"
#include "trafficlightstatic.h"
#include "trafficlightadaptive.h"
#include <cstdio>
#include <map>

using namespace std;

static const string COLOR_NONE   ="\e[00m";
static const string COLOR_RED    ="\e[01;31m";
static const string COLOR_ORANGE ="\e[01;43m";
static const string COLOR_GREEN  ="\e[01;32m";
static const string COLOR_BLUE   ="\e[01;34m";
static const string COLOR_YELLOW ="\e[01;33m";
static const string COLOR_WHITE  ="\e[01;37m";
static const string COLOR_CYAN   ="\e[01;36m";

class Utils {

public:

    
    static ITrafficLight * getTrafficLightEast() {
        if (east == NULL) {
            east = getTrafficLigth(ID_EAST);
        }
        return east;
    }

    static ITrafficLight * getTrafficLightWest() {
        if (west == NULL) {
            west = getTrafficLigth(ID_WEST);
        }
        return west;
    }

    static string to_string(double x) {
        char buffer [50];
        sprintf(buffer, "%f", x);
        return string(buffer);
    }

    static string to_string(unsigned x) {
        char buffer [50];
        sprintf(buffer, "%d", x);
        return string(buffer);
    }
    static string to_string(int x) {
        char buffer [50];
        sprintf(buffer, "%d", x);
        return string(buffer);
    }

    static void log(string message) {
        cout << COLOR_WHITE << Time << COLOR_NONE << ": " << message << endl;
    }

    static Histogram* getHistogram() {
        if (hist == NULL) {
            hist = new Histogram("cekani", 0, 3, 100);
        }
        return hist;
    }

    static TrafficLightType type;
    static map<Car *, double> connectionRoadToWest; /* car, time when left junction */
    static map<Car *, double> connectionRoadToEast; /* car, time when left junction */

    static unsigned countOfCarsCommingBellow(map<Car *, double> & road, double seconds) {
        unsigned count = 0;
        for (map<Car *, double>::iterator it = road.begin(); it != road.end(); it++) {
            if (Time - it->second >= seconds)
                count++;
        }
        return count;
    }

    static void deregisterFromConnectionRoads(Car *c) {
        connectionRoadToEast.erase(c);
        connectionRoadToWest.erase(c);
    }

protected:
    static ITrafficLight *east;
    static ITrafficLight *west;
    static Histogram* hist;

    static ITrafficLight* getTrafficLigth(TrafficLightIdentification id) {
        ITrafficLight *rv;
        if (Utils::type == ADAPTIVE) {
            rv = new TrafficLightAdaptive(id);
        } else {
            rv = new TrafficLightStatic(id);
        }
        return rv;
    }



};

//ITrafficLight* TrafficLights::east = NULL;

#endif // UTILS_H
