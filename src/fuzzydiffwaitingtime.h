/*
 * File:   fuzzydiffwaitingtime.h
 * Author: daniel bokis, tomas dohnalek
 *
 * Created on 26. listopad 2014, 14:36
 */



#ifndef FUZZYDIFFWAITINGTIME_H
#define FUZZYDIFFWAITINGTIME_H

#include "ifuzzy.h"

class FuzzyDiffWaitingTime: public IFuzzy
{
public:
    FuzzyDiffWaitingTime();

    virtual void createInferenceRules(Engine *engine);
    virtual InputVariable * createFWaitTime();
};

#endif // FUZZYDIFFWAITINGTIME_H
