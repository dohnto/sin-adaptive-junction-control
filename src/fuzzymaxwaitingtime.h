/*
 * File:   fuzzymaxwaitingtime.h
 * Author: daniel bokis, tomas dohnalek
 *
 * Created on 26. listopad 2014, 14:36
 */

#ifndef FUZZYMAXWAITINGTIME_H
#define FUZZYMAXWAITINGTIME_H

#include "ifuzzy.h"

class FuzzyMaxWaitingTime : public IFuzzy
{
public:
    FuzzyMaxWaitingTime();

    virtual void createInferenceRules(Engine *engine);
    virtual InputVariable * createFWaitTime();
};

#endif // FUZZYMAXWAITINGTIME_H
