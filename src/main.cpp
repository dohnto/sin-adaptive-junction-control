/*
 * File:   main.cpp
 * Author: daniel bokis, tomas dohnalek
 *
 * Created on 26. listopad 2014, 14:36
 */


#include <iostream>
#include <simlib.h>

#include "utils.h"
#include "cargenerator.h"
#include "fuzzy.h"


ITrafficLight* Utils::east;
ITrafficLight* Utils::west;
Histogram *Utils::hist;
TrafficLightType Utils::type;
map<Car *, double> Utils::connectionRoadToWest;
map<Car *, double> Utils::connectionRoadToEast;

using namespace std;

void help(int argc, char **argv) {
    cout << "Usage: " << argv[0] << " [static|adaptive]" << endl;
}

int main(int argc, char **argv) {

    if (argc != 2) {
        help(argc, argv);
        return 0;
    }

    TrafficLightType type;

    if (string(argv[1]) == "static") {
        type = STATIC;
    } else if (string(argv[1]) == "adaptive") {
        type = ADAPTIVE;
    } else {
        help(argc, argv);
        return 0;
    }




    SetOutput("model.out");
    Init    (0,86400);

    //RandomSeed(time(NULL));

    Utils::type = type;

    ITrafficLight* west = Utils::getTrafficLightWest();
    ITrafficLight* east = Utils::getTrafficLightEast();

    new CarGenerator(HORIZONTAL, WEST, east, 6);
    new CarGenerator(HORIZONTAL, EAST, west, 6);

    new CarGenerator(VERTICAL_EAST, SOUTH, east, 10);
    new CarGenerator(VERTICAL_EAST, NORTH, east, 10);

    new CarGenerator(VERTICAL_WEST, SOUTH, west, 9);
    new CarGenerator(VERTICAL_WEST, NORTH, west, 9);

    east->Activate();
    west->Activate();

    Run();

    Utils::log(COLOR_GREEN + "mean waiting time is: " + Utils::to_string(Utils::getHistogram()->stat.MeanValue()) + " +- " + Utils::to_string(Utils::getHistogram()->stat.StdDev()));
    Utils::log(COLOR_GREEN + "max waiting time is: "  + Utils::to_string(Utils::getHistogram()->stat.Max()));
    Utils::getHistogram()->Output();
    
    return 0;
}
