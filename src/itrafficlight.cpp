/*
 * File:   itrafficlight.cpp
 * Author: daniel bokis, tomas dohnalek
 *
 * Created on 26. listopad 2014, 14:36
 */

#include "itrafficlight.h"
#include "utils.h"
#include "Car.h"
#include <string>





ITrafficLight::ITrafficLight(TrafficLightIdentification id): id(id), timeWhenStatusChanged(Time) {
    status = GOVERTICAL;
    Utils::log(string("New ") + getLogName()+" spawned");
    printStatus(status);
}

bool ITrafficLight::registerCar(Car *c) {
    Utils::log(string("Car ") + Utils::to_string(c->id) +" registers at " + getLogName());
    Utils::deregisterFromConnectionRoads(c);
    bool canCarGo = false;
    switch(c->direct) {
        case NORTH:
            if (status == GOVERTICAL && qSouth.size() == 0 && Time - timeWhenStatusChanged >= TRAFFIC_LIGHT_PROTECTION_INTERVAL) {
                canCarGo = true;
            } else {
                qSouth.push_back(c);
            }
            break;
        case SOUTH:
            if (status == GOVERTICAL && qNorth.size() == 0 && Time - timeWhenStatusChanged >= TRAFFIC_LIGHT_PROTECTION_INTERVAL) {
                canCarGo = true;
            } else {
                qNorth.push_back(c);
            }
            break;
        case EAST:
            if (status == GOHORIZONTAL && qWest.size() == 0 && Time - timeWhenStatusChanged >= TRAFFIC_LIGHT_PROTECTION_INTERVAL) {
                canCarGo = true;
                if (id == ID_WEST)
                    Utils::connectionRoadToEast[c] = Time;
            } else {
                qWest.push_back(c);
            }
            break;
        case WEST:
            if (status == GOHORIZONTAL && qEast.size() == 0 && Time - timeWhenStatusChanged >= TRAFFIC_LIGHT_PROTECTION_INTERVAL) {
                canCarGo = true;
                if (id == ID_EAST)
                    Utils::connectionRoadToWest[c] = Time;
            } else {
                qEast.push_back(c);
            }

            break;
    }

    return canCarGo;
}

ITrafficLight * ITrafficLight::getNeighbour() {
    if (id == ID_WEST) {
        return Utils::getTrafficLightEast();
    } else {
        return Utils::getTrafficLightWest();
    }
}

void ITrafficLight::goFromQueue(deque<Car*> &q, map<Car *, double> & road, bool putToRoad) {
    if (!q.empty()) {
        Car *c = q.front();
        if (c != NULL) {
            if (putToRoad) {
                road[c] = Time;
            }
            c->Activate();
        }
        q.pop_front();
    }
}

void ITrafficLight::goGreen() {
    if (status == GOHORIZONTAL) {
        bool leavesToConnectionRoad = false;
        if (id == ID_EAST) {
            leavesToConnectionRoad = true;
        }
        goFromQueue(qEast, Utils::connectionRoadToWest, leavesToConnectionRoad);

        leavesToConnectionRoad = false;
        if (id == ID_WEST) {
            leavesToConnectionRoad = true;
        }
        goFromQueue(qWest, Utils::connectionRoadToEast, leavesToConnectionRoad);
    } else {
        goFromQueue(qNorth,Utils::connectionRoadToEast, false);
        goFromQueue(qSouth,Utils::connectionRoadToEast, false);
    }
    
}

void ITrafficLight::printStatus(LightStatus status) {
    string rv = getLogName();
    if (status == GOHORIZONTAL) {
        rv += COLOR_RED + " go horizontal";
    } else {
        rv += COLOR_BLUE + " go vertical";
    }
    Utils::log(rv);
}


string spaces(unsigned n, char c) {
    return string(n, c);
}

string spaces(unsigned n) {
    return spaces(n, ' ');
}

unsigned coloredSize(string str) {
    return str.size() - COLOR_RED.size() - COLOR_NONE.size();
}

void ITrafficLight::printMap() {
    if (id == ID_WEST) {
        ITrafficLight *neighbour = getNeighbour();
        /*
         *            [      ]
         *            \/
         *              <- [        ]
         *   [    ] ->
         *              ^
         *              [      ]
         * */
        cout << getLogName() << "\t\t" << neighbour->getLogName() << endl;
        cout << COLOR_NONE;

        string colorQNorth = (status == GOHORIZONTAL)
                ? COLOR_RED
                : (Time - timeWhenStatusChanged >= TRAFFIC_LIGHT_PROTECTION_INTERVAL)
                  ? COLOR_GREEN
                  : COLOR_ORANGE;

        string colorQWest = (status == GOVERTICAL)
                ? COLOR_RED
                : (Time - timeWhenStatusChanged >= TRAFFIC_LIGHT_PROTECTION_INTERVAL)
                  ? COLOR_GREEN
                  : COLOR_ORANGE;


        string colorQNorthNeighbour = (neighbour->status == GOHORIZONTAL)
                ? COLOR_RED
                : (Time - neighbour->timeWhenStatusChanged >= TRAFFIC_LIGHT_PROTECTION_INTERVAL)
                  ? COLOR_GREEN
                  : COLOR_ORANGE;

        string colorQWestNeighbour = (neighbour->status == GOVERTICAL)
                ? COLOR_RED
                : (Time - neighbour->timeWhenStatusChanged >= TRAFFIC_LIGHT_PROTECTION_INTERVAL)
                  ? COLOR_GREEN
                  : COLOR_ORANGE;

        string qn  = colorQNorth + getQueAsText(qNorth) + COLOR_NONE;
        string qn2 = colorQNorthNeighbour + neighbour->getQueAsText(neighbour->qNorth) + COLOR_NONE;

        string qe  = colorQWest + getQueAsText(qEast) + COLOR_NONE;
        string qe2 = colorQWestNeighbour + neighbour->getQueAsText(neighbour->qEast) + COLOR_NONE;

        string qw  = colorQWest + getQueAsText(qWest, true) + COLOR_NONE;
        string qw2 = colorQWestNeighbour + neighbour->getQueAsText(neighbour->qWest, true) + COLOR_NONE;

        string qs  = colorQNorth + getQueAsText(qSouth) + COLOR_NONE;
        string qs2 = colorQNorthNeighbour + neighbour->getQueAsText(neighbour->qSouth) + COLOR_NONE;


        //cout << qw << " = " << qw.size() << endl;

        string j11 = spaces(coloredSize(qw)) + qn;
        string j12 = spaces(coloredSize(qw)) + "\\/<" + qe;
        string j13 = qw + ">/\\";
        string j14 = spaces(coloredSize(qw)+1) + qs;

        unsigned max = j11.size();
        if (j12.size() > max) max = j12.size();
        if (j14.size() > max) max = j14.size();
        if (j14.size() > max) max = j13.size();
        max += 10;

        string j21 = spaces(coloredSize(qw2)) + qn2;
        string j22 = spaces(coloredSize(qw2), '<') + "\\/<" + qe2;
        string j23 = qw2 + ">/\\";
        string j24 = spaces(coloredSize(qw2)+1) + qs2;

        cout << j11 + spaces(max - j11.size());
        cout << j21 ;
        cout << endl;

        cout << j12 + spaces(max - j12.size(), '<');
        cout << j22;
        cout << endl;

        cout << j13  + spaces(max - j13.size(), '>');
        cout << j23 ;
        cout << endl;

        cout << j14  + spaces(max - j14.size());
        cout << j24 ;
        cout << endl;
        cout << neighbour->getLogName() << " " << Utils::connectionRoadToEast.size() << endl;
        cout << getLogName() << " " << Utils::connectionRoadToWest.size() << endl;
    }
}

void ITrafficLight::Behavior() {
    while(1) {
        if (id == ID_WEST)
        cout << COLOR_NONE << spaces(100, '-') << endl;

        if (Time - timeWhenStatusChanged >= TRAFFIC_LIGHT_PROTECTION_INTERVAL) {
            goGreen();
        }

        printMap();
        status = getNextStatus();

        //printStatus(status);

        Wait(TRAFFIC_LIGHT_CHECK_SITUATION_EVERY);

        /*printQue(qNorth, getLogName() + " North");
        printQue(qSouth, getLogName() + " South");
        printQue(qWest, getLogName() + " West ");
        printQue(qEast, getLogName() + " East ");*/
    }
}

string ITrafficLight::getQueAsText(deque<Car*> &q, bool reverse) {
    string text;
    text += "[ ";
    if (reverse) {
        for (deque<Car*>::reverse_iterator it = q.rbegin(); it != q.rend(); it++) {
           text += Utils::to_string((*it)->id) + " ";
        }
    } else {
         for (deque<Car*>::iterator it = q.begin(); it != q.end(); it++) {
            text += Utils::to_string((*it)->id) + " ";
         }
    }
     text += "]";
     return text;
}

void ITrafficLight::printQue(deque<Car*> & q, string name) {
    string message = name + " ";
    message += getQueAsText(q);

     Utils::log(message);

}

string ITrafficLight::getLogName() {
    string rv("");
    if (id == ID_EAST) {
        rv += COLOR_YELLOW;
    } else {
        rv += COLOR_CYAN;
    }
    rv += "traffic light" + COLOR_NONE;
    return rv;
}
