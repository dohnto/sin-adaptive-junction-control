/*
 * File:   types.h
 * Author: daniel bokis, tomas dohnalek
 *
 * Created on 26. listopad 2014, 14:36
 */

#ifndef TYPES_H
#define TYPES_H

#include <simlib.h>
#include <iostream>

enum TrafficLightType {
    STATIC,
    ADAPTIVE
};

enum TrafficLightIdentification {
    ID_WEST,
    ID_EAST
};

enum LightStatus {
    GOHORIZONTAL,
    GOVERTICAL
};

enum Routes {
    HORIZONTAL,
    VERTICAL_EAST,
    VERTICAL_WEST
};

enum Direction {
    NORTH,
    SOUTH,
    WEST,
    EAST
};

enum TrafficLightAdaptiveConnectionCalculation {
    IGNORE,
    ALL,
    MEAN,
};

enum TrafficLightAdaptiveFuzzLogic {
    DIFF_TIME,
    RED_ONLY,
};

#endif // TYPES_H
