/* 
 * File:   Car.h
 * Author: daniel bokis, tomas dohnalek
 *
 * Created on 26. listopad 2014, 14:36
 */

#include "types.h"
class ITrafficLight;

#ifndef CAR_H
#define	CAR_H

class Car : public Process {
public:
    Car(Routes r, Direction d, ITrafficLight *l);
    
    void Behavior();
    
    double getStartWaited();
    
    Routes route;
    Direction direct;

    int id;
    
private:
    double waited;
    ITrafficLight *light;
    static int i;
    double startWaiting;
};

#endif	/* CAR_H */

