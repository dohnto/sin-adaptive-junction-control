/*
 * File:   fuzzydiffwaitingtime.cpp
 * Author: daniel bokis, tomas dohnalek
 *
 * Created on 26. listopad 2014, 14:36
 */

#include "fuzzydiffwaitingtime.h"
#include "config.h"

FuzzyDiffWaitingTime::FuzzyDiffWaitingTime()
    : IFuzzy()
{
}


InputVariable * FuzzyDiffWaitingTime::createFWaitTime() {
  InputVariable* wait = new InputVariable;
      wait->setName("WAIT");
      wait->setRange(DFMIN, DFMAX);

      wait->addTerm(new Trapezoid("R", DFMIN, DFMIN, DFLOW, DFMED)); // on red light there is a car which waits longer
      wait->addTerm(new Triangle ("S", DFLOW, DFMED, DFHIG));   // its more less the same
      wait->addTerm(new Trapezoid("G", DFMED, DFHIG, DFMAX, DFMAX)); // green waits longer
  return wait;

}

void FuzzyDiffWaitingTime::createInferenceRules(Engine *engine) {

    // small
    RuleBlock* ruleblock = new RuleBlock;

    ruleblock->addRule(Rule::parse("if RED is S and GREEN is S and WAIT is R then CHANGE is Y", engine));
    ruleblock->addRule(Rule::parse("if RED is S and GREEN is S and WAIT is S then CHANGE is N", engine));
    ruleblock->addRule(Rule::parse("if RED is S and GREEN is S and WAIT is G then CHANGE is N", engine));

    ruleblock->addRule(Rule::parse("if RED is S and GREEN is M and WAIT is R then CHANGE is Y", engine));
    ruleblock->addRule(Rule::parse("if RED is S and GREEN is M and WAIT is S then CHANGE is N", engine));
    ruleblock->addRule(Rule::parse("if RED is S and GREEN is M and WAIT is G then CHANGE is N", engine));

    ruleblock->addRule(Rule::parse("if RED is S and GREEN is L and WAIT is R then CHANGE is Y", engine));
    ruleblock->addRule(Rule::parse("if RED is S and GREEN is L and WAIT is S then CHANGE is N", engine));
    ruleblock->addRule(Rule::parse("if RED is S and GREEN is L and WAIT is G then CHANGE is N", engine));


    ruleblock->addRule(Rule::parse("if RED is M and GREEN is S and WAIT is R then CHANGE is Y", engine));
    ruleblock->addRule(Rule::parse("if RED is M and GREEN is S and WAIT is S then CHANGE is Y", engine));
    ruleblock->addRule(Rule::parse("if RED is M and GREEN is S and WAIT is G then CHANGE is N", engine));

    ruleblock->addRule(Rule::parse("if RED is M and GREEN is M and WAIT is R then CHANGE is Y", engine));
    ruleblock->addRule(Rule::parse("if RED is M and GREEN is M and WAIT is S then CHANGE is N", engine));
    ruleblock->addRule(Rule::parse("if RED is M and GREEN is M and WAIT is G then CHANGE is N", engine));

    ruleblock->addRule(Rule::parse("if RED is M and GREEN is L and WAIT is R then CHANGE is Y", engine));
    ruleblock->addRule(Rule::parse("if RED is M and GREEN is L and WAIT is S then CHANGE is N", engine));
    ruleblock->addRule(Rule::parse("if RED is M and GREEN is L and WAIT is G then CHANGE is N", engine));


    ruleblock->addRule(Rule::parse("if RED is L and GREEN is S and WAIT is R then CHANGE is Y", engine));
    ruleblock->addRule(Rule::parse("if RED is L and GREEN is S and WAIT is S then CHANGE is Y", engine));
    ruleblock->addRule(Rule::parse("if RED is L and GREEN is S and WAIT is G then CHANGE is N", engine));

    ruleblock->addRule(Rule::parse("if RED is L and GREEN is M and WAIT is R then CHANGE is Y", engine));
    ruleblock->addRule(Rule::parse("if RED is L and GREEN is M and WAIT is S then CHANGE is Y", engine));
    ruleblock->addRule(Rule::parse("if RED is L and GREEN is M and WAIT is G then CHANGE is N", engine));

    ruleblock->addRule(Rule::parse("if RED is L and GREEN is L and WAIT is R then CHANGE is Y", engine));
    ruleblock->addRule(Rule::parse("if RED is L and GREEN is L and WAIT is S then CHANGE is N", engine));
    ruleblock->addRule(Rule::parse("if RED is L and GREEN is L and WAIT is G then CHANGE is N", engine));


    ruleblock->setConjunction(new Minimum);
    ruleblock->setDisjunction(new Maximum);
    ruleblock->setActivation(new Minimum);
    engine->addRuleBlock(ruleblock);
}

