/*
 * File:   trafficlightstatic.h
 * Author: daniel bokis, tomas dohnalek
 *
 * Created on 26. listopad 2014, 14:36
 */

#ifndef TRAFFICLIGHTSTATIC_H
#define TRAFFICLIGHTSTATIC_H

#include "itrafficlight.h"
#include "utils.h"
#include "config.h"


class TrafficLightStatic: public ITrafficLight {
public:
    TrafficLightStatic(TrafficLightIdentification id): ITrafficLight(id) {}

    virtual LightStatus getNextStatus() {
        double diffTimeToChange = 0;
        if (status == GOHORIZONTAL) {
                diffTimeToChange = TRAFFIC_LIGHT_STATIC_HORIZONTAL_SWITCH + TRAFFIC_LIGHT_PROTECTION_INTERVAL;
        } else {
                diffTimeToChange = TRAFFIC_LIGHT_STATIC_VERTICAL_SWITCH + TRAFFIC_LIGHT_PROTECTION_INTERVAL;
        }


        if (Time - timeWhenStatusChanged > diffTimeToChange) {
            timeWhenStatusChanged = Time;
            if (status == GOHORIZONTAL) {
                return GOVERTICAL;
            } else {
                return GOHORIZONTAL;
            }
        } else {
            return status;
        }
    }

private:
};

#endif
