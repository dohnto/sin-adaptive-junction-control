/*
 * File:   ifuzzy.h
 * Author: daniel bokis, tomas dohnalek
 *
 * Created on 26. listopad 2014, 14:36
 */

#ifndef IFUZZY_H
#define	IFUZZY_H

#include "types.h"
#include <iostream>

using namespace std;
#include <fl/Headers.h>

using namespace fl;

class IFuzzy {
public:
    IFuzzy();
    static InputVariable * createFRedQueue();
    static InputVariable * createFGreenQueue();
    virtual InputVariable * createFWaitTime()  = 0;
    static OutputVariable * createFChange();
    virtual void createInferenceRules(Engine *engine) = 0;
    void finishInit() {
        engine = new Engine("crossing");
        redQueue = createFRedQueue();
        engine->addInputVariable(redQueue);
        greenQueue = createFGreenQueue();
        engine->addInputVariable(greenQueue);
        waitTime = createFWaitTime();
        engine->addInputVariable(waitTime);

        change = createFChange();
        engine->addOutputVariable(change);

        createInferenceRules(engine);

        cout << getChange(0,0,0) << endl;
        cout << getChange(1,1000,1) << endl;
        //exit(2);
    }
    
    bool getChange(unsigned redQueSize, unsigned greenQueSize, double maxWaitTime);
protected:
    InputVariable *redQueue;
    InputVariable *greenQueue;
    InputVariable *waitTime;
    OutputVariable *change;
    Engine* engine;
            
};

#endif	/* FUZZY_H */

