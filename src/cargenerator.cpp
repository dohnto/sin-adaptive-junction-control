/*
 * File:   cargenerator.cpp
 * Author: daniel bokis, tomas dohnalek
 *
 * Created on 26. listopad 2014, 14:36
 */

#include "cargenerator.h"
#include "Car.h"
#include "config.h"

CarGenerator::CarGenerator(Routes route, Direction direction, ITrafficLight *trafficlight, double meanValue):
    route(route), direction(direction), trafficlight(trafficlight), meanValue(meanValue)
{
    Activate(Time + CAR_GENERATOR_MINIMUM_SEC + Exponential(meanValue));
}

void CarGenerator::Behavior()
{
    (new Car(route, direction, trafficlight))->Activate();
    Activate(Time + CAR_GENERATOR_MINIMUM_SEC + Exponential(meanValue));
}
