/*
 * File:   itrafficlight.h
 * Author: daniel bokis, tomas dohnalek
 *
 * Created on 26. listopad 2014, 14:36
 */

#ifndef ITRAFFICLIGHT
#define ITRAFFICLIGHT
#include <deque>
#include <map>

class Car;
#include "types.h"

using namespace std;

class ITrafficLight : public Process {
public:    
    ITrafficLight(TrafficLightIdentification);
    bool registerCar(Car *c);
    void makeDecisions();
    ITrafficLight * getNeighbour();

    void Behavior();
    
    TrafficLightIdentification id;
    
    string getLogName();

    virtual LightStatus getNextStatus() = 0;
protected:
    void goGreen();
    void goFromQueue(deque<Car*> &q, map<Car *, double> & road, bool putToRoad);
    double timeWhenStatusChanged;
    LightStatus status;
    
    deque<Car*> qNorth;
    deque<Car*> qSouth;
    deque<Car*> qEast;
    deque<Car*> qWest;

    void printQue(deque<Car*> &q, string name);
    void printStatus(LightStatus status);
    void printMap();
    string getQueAsText(deque<Car*> &q  , bool reverse = false);
};

#endif
