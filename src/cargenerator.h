/*
 * File:   cargeneator.h
 * Author: daniel bokis, tomas dohnalek
 *
 * Created on 26. listopad 2014, 14:36
 */

#ifndef CARGENERATOR_H
#define CARGENERATOR_H

#include "types.h"
#include "itrafficlight.h"

class CarGenerator: public Event {
public:
    CarGenerator(Routes route, Direction direction, ITrafficLight *trafficlight, double meanValue);
    void Behavior();
private:

    Routes route;
    Direction direction;
    ITrafficLight *trafficlight;
    double meanValue;
};

#endif
