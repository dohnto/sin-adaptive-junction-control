/*
 * File:   fuzzymaxwaitingtime.cpp
 * Author: daniel bokis, tomas dohnalek
 *
 * Created on 26. listopad 2014, 14:36
 */

#include "fuzzymaxwaitingtime.h"
#include "config.h"

FuzzyMaxWaitingTime::FuzzyMaxWaitingTime()
    : IFuzzy()
{

}

InputVariable * FuzzyMaxWaitingTime::createFWaitTime() {
  InputVariable* wait = new InputVariable;
      wait->setName("WAIT");
      wait->setRange(0.000, RQMAX);
      wait->addTerm(new Trapezoid("S", 0.000, 0.000, WTLOW, WTMED));
      wait->addTerm(new Triangle ("M", 0.000, WTMED, WTHIG));
      wait->addTerm(new Trapezoid("L", WTMED, WTHIG, WTMAX, WTMAX));
  return wait;

}

void FuzzyMaxWaitingTime::createInferenceRules(Engine *engine) {

    // small
    RuleBlock* ruleblock = new RuleBlock;
    ruleblock->addRule(Rule::parse("if RED is S and GREEN is S and WAIT is S then CHANGE is N", engine));
    ruleblock->addRule(Rule::parse("if RED is S and GREEN is S and WAIT is M then CHANGE is N", engine));
    ruleblock->addRule(Rule::parse("if RED is S and GREEN is S and WAIT is L then CHANGE is Y", engine));

    ruleblock->addRule(Rule::parse("if RED is S and GREEN is M and WAIT is S then CHANGE is N", engine));
    ruleblock->addRule(Rule::parse("if RED is S and GREEN is M and WAIT is M then CHANGE is N", engine));
    ruleblock->addRule(Rule::parse("if RED is S and GREEN is M and WAIT is L then CHANGE is Y", engine));

    ruleblock->addRule(Rule::parse("if RED is S and GREEN is L and WAIT is S then CHANGE is N", engine));
    ruleblock->addRule(Rule::parse("if RED is S and GREEN is L and WAIT is M then CHANGE is N", engine));
    ruleblock->addRule(Rule::parse("if RED is S and GREEN is L and WAIT is L then CHANGE is N", engine));


    ruleblock->addRule(Rule::parse("if RED is M and GREEN is S and WAIT is S then CHANGE is Y", engine));
    ruleblock->addRule(Rule::parse("if RED is M and GREEN is S and WAIT is M then CHANGE is Y", engine));
    ruleblock->addRule(Rule::parse("if RED is M and GREEN is S and WAIT is L then CHANGE is Y", engine));

    ruleblock->addRule(Rule::parse("if RED is M and GREEN is M and WAIT is S then CHANGE is N", engine));
    ruleblock->addRule(Rule::parse("if RED is M and GREEN is M and WAIT is M then CHANGE is N", engine));
    ruleblock->addRule(Rule::parse("if RED is M and GREEN is M and WAIT is L then CHANGE is Y", engine));

    ruleblock->addRule(Rule::parse("if RED is M and GREEN is L and WAIT is S then CHANGE is N", engine));
    ruleblock->addRule(Rule::parse("if RED is M and GREEN is L and WAIT is M then CHANGE is Y", engine));
    ruleblock->addRule(Rule::parse("if RED is M and GREEN is L and WAIT is L then CHANGE is Y", engine));


    ruleblock->addRule(Rule::parse("if RED is L and GREEN is S and WAIT is S then CHANGE is Y", engine));
    ruleblock->addRule(Rule::parse("if RED is L and GREEN is S and WAIT is M then CHANGE is Y", engine));
    ruleblock->addRule(Rule::parse("if RED is L and GREEN is S and WAIT is L then CHANGE is Y", engine));

    ruleblock->addRule(Rule::parse("if RED is L and GREEN is M and WAIT is S then CHANGE is Y", engine));
    ruleblock->addRule(Rule::parse("if RED is L and GREEN is M and WAIT is M then CHANGE is Y", engine));
    ruleblock->addRule(Rule::parse("if RED is L and GREEN is M and WAIT is L then CHANGE is Y", engine));

    ruleblock->addRule(Rule::parse("if RED is L and GREEN is L and WAIT is S then CHANGE is N", engine));
    ruleblock->addRule(Rule::parse("if RED is L and GREEN is L and WAIT is M then CHANGE is Y", engine));
    ruleblock->addRule(Rule::parse("if RED is L and GREEN is L and WAIT is L then CHANGE is Y", engine));


    ruleblock->setConjunction(new Minimum);
    ruleblock->setDisjunction(new Maximum);
    ruleblock->setActivation(new Minimum);
    engine->addRuleBlock(ruleblock);
}

