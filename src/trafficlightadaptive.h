/*
 * File:   trafficlightadaptive.h
 * Author: daniel bokis, tomas dohnalek
 *
 * Created on 26. listopad 2014, 14:36
 */

#ifndef TRAFFICLIGHTADAPTIVE_H
#define TRAFFICLIGHTADAPTIVE_H

#include "itrafficlight.h"
#include "fuzzydiffwaitingtime.h"
#include "fuzzymaxwaitingtime.h"
#include "Car.h"

class TrafficLightAdaptive: public ITrafficLight {
public:
    TrafficLightAdaptive(TrafficLightIdentification id);

    virtual LightStatus getNextStatus();
    
    double getMaxTimeQ(deque<Car*> &q) {
        double maxTime = 0; 
        for(deque<Car*>::iterator i=q.begin(); i!=q.end(); i++)
        {
            double waited = Time - (*i)->getStartWaited();
            if (waited > maxTime)
                maxTime = waited;
        }
        
        return maxTime;
    }
protected:
    IFuzzy *fuzzy;
    double timeWhenStatusChanged;
};

#endif
