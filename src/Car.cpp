/*
 * File:   Car.cpp
 * Author: daniel bokis, tomas dohnalek
 *
 * Created on 26. listopad 2014, 14:36
 */

#include "Car.h"
#include "itrafficlight.h"
#include "utils.h"
#include <stdlib.h>

#include "config.h"

int Car::i;

Car::Car(Routes r, Direction d, ITrafficLight *l)
{
    id = i++;
    waited = 0;
    route = r;
    direct = d;
    light = l;

    Utils::log(string("New car spawned with id = ") + Utils::to_string(id));
}

double Car::getStartWaited() {
    return startWaiting;
}

void Car::Behavior()
{
    startWaiting = Time;

    // register semafor
    bool canGo = light->registerCar(this);
    if (! canGo) {
        Passivate();
    }

    waited += Time - startWaiting;
    startWaiting = 0;
    Utils::getHistogram()->operator()(waited);

    if (route == HORIZONTAL) {
        Wait(Normal(BETWEEN_JUNCTIONS_MEAN_SEC, BETWEEN_JUNCTIONS_VARIANCE));
        startWaiting = Time;

        // register druhy semafor
        light = light->getNeighbour();
        light->registerCar(this);

        Passivate();
        waited = Time - startWaiting;
        Utils::getHistogram()->operator()(waited);
    }

    // statistika posli waited
    Utils::log(string("Car ")  + Utils::to_string(id) + " is leaving system with waiting time: " + Utils::to_string(waited));
}

