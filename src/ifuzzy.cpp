/*
 * File:   ifuzzy.cpp
 * Author: daniel bokis, tomas dohnalek
 *
 * Created on 26. listopad 2014, 14:36
 */

#include "ifuzzy.h"
#include "utils.h"
#include "config.h"

IFuzzy::IFuzzy() {

}


InputVariable * IFuzzy::createFRedQueue() {
    InputVariable* red = new InputVariable;
        red->setName("RED");
        red->setRange(0.000, RQMAX);
        red->addTerm(new Triangle ("S", 0.000, 0.000, RQLOW));
        red->addTerm(new Triangle ("M", 0.000, RQLOW, RQMED));
        red->addTerm(new Trapezoid("L", RQLOW, RQMED, RQMAX, RQMAX));
    return red;
}

InputVariable * IFuzzy::createFGreenQueue() {

  InputVariable* green = new InputVariable;
      green->setName("GREEN");
      green->setRange(0.000, GQMAX);
      green->addTerm(new Triangle ("S", 0.000, 0.000, GQLOW));
      green->addTerm(new Triangle ("M", 0.000, GQLOW, GQMED));
      green->addTerm(new Trapezoid("L", GQLOW, GQMED, GQMAX, GQMAX));
  return green;
}

OutputVariable * IFuzzy::createFChange() {

    OutputVariable* change = new OutputVariable;
        change->setName("CHANGE");
        change->setRange(0.000, 1);
        change->addTerm(new Triangle ("N", 0.000, 0.000, 1.000));
        change->addTerm(new Triangle ("Y", 0.000, 1.000, 1.000));
        change->fuzzyOutput()->setAccumulation(new Maximum);
        change->setDefuzzifier(new MeanOfMaximum(500));
        change->setDefaultValue(0);
    return change;
}

bool IFuzzy::getChange(unsigned redQueSize, unsigned greenQueSize, double maxWaitTime) {
    redQueue->setInputValue(redQueSize);
    greenQueue->setInputValue(greenQueSize);
    waitTime->setInputValue(maxWaitTime);
    
    engine->process();
    double output = change->getOutputValue();
    return ( output >= 0.5);
}

