/*
 * File:   config.h
 * Author: daniel bokis, tomas dohnalek
 *
 * Created on 26. listopad 2014, 14:36
 */



#ifndef CONFIG_H
#define CONFIG_H

#include "types.h"

const unsigned BETWEEN_JUNCTIONS_MEAN_SEC = 15;
const unsigned BETWEEN_JUNCTIONS_VARIANCE = 2;

const unsigned CAR_GENERATOR_MINIMUM_SEC = 1;

const unsigned TRAFFIC_LIGHT_CHECK_SITUATION_EVERY = 2;
const unsigned TRAFFIC_LIGHT_MIN_TIME_BETWEEN_SWITCH = 10;
const unsigned TRAFFIC_LIGHT_PROTECTION_INTERVAL = 6;

const unsigned TRAFFIC_LIGHT_STATIC_HORIZONTAL_SWITCH = 6;
const unsigned TRAFFIC_LIGHT_STATIC_VERTICAL_SWITCH   = 4;

const TrafficLightAdaptiveConnectionCalculation TRAFFIC_LIGHT_ADAPTIVE_CONNECTION_CALCULATION = MEAN;
const TrafficLightAdaptiveFuzzLogic TRAFFIC_LIGHT_ADAPTIVE_FUZZY_LOGIC = DIFF_TIME;

// fuzzy
const static int RQLOW = 5;
const static int RQMED = 10;
const static int RQMAX = 100;

const static int GQLOW = 4;
const static int GQMED = 8;
const static int GQMAX = 100;

const static int WTLOW = 10;
const static int WTMED = 20;
const static int WTHIG = 30;
const static int WTMAX = 300;

const static int DFMIN = -100;
const static int DFLOW = -2;
const static int DFMED = 0;
const static int DFHIG = 2;
const static int DFMAX = 100;

#endif // CONFIG_H
