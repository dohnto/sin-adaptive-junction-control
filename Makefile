all:
	$(MAKE) -C src/
	cp src/sin .
clean:
	$(MAKE) -C src/ clean

pack zip:
	zip -MM 1-xbokis00-xdohna25-50-50.zip INSTALL RUN src/Makefile src/*.cpp src/*.h Makefile documentation.pdf *.patch

install:
	./INSTALL

run:
	./RUN

